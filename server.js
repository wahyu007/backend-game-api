const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { json } = require("body-parser");
const app = express();

// var corsOptions = {
//     origin: "http://localhost:8080"
// };

app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended : true
}));

app.get('/', (req, res) =>{
    res.send({
        status : "oke"
    });
});

const db = require('./app/models');
db.sequelize.sync();

// db.sequelize.sync({
//     force : true
// }).then(() => { console.log("Drop and re-sync db")})

require('./app/routes/routes')(app);

// console.log(db);
app.listen(8080, () => {
    console.log(`Server is running on port 8080`)
})