module.exports = app => {
    const users = require('../controllers/user');
    const games = require('../controllers/game');
    const logs = require('../controllers/log');

    let router = require('express').Router();

    router.post('/', users.create);
    router.get('/', users.findAll);
    router.get('/:id', users.findOne);
    router.put('/:id', users.update);
    router.delete('/:id', users.delete);
    router.post('/login', users.login);
    router.get('/games/all', games.findAllGame);
    router.post('/games', games.create);
    router.get('/logs/all', logs.findAllLog);
    
    app.use('/api/v1/users', router);
}