const db = require('../models/index');
const Game = db.games;
const Log = db.logs;
const Op = db.Sequelize.Op;
// console.log("apa");

exports.create = (req, res) => {
    if(!req.body.id_user) {
        res.status(400).send({
            message : "Content can not be empty"
        });
        return;
    }

    const game = {
        id_user : req.body.id_user,
        score : req.body.score,
        top_score : req.body.top_score
    }

    Game.create(game)
    .then(data => {
        res.send(data);
    })
    .catch( err => {
        res.status(500).send({
            status : "error",
            message : err.message || "Some error ocured while creating the game."
        });
    })

    Log.create({
        id_user : req.body.id_user,
        status : "create new data game" 
    });
} 

exports.findAllGame = (req, res) => {
    Game.findAll()
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                status : "error on games",
                message : err.message || "Some error while retriving games."
            })
        })
}

