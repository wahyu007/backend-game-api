const db = require('../models/index');
const Log = db.logs;
const Op = db.Sequelize.Op;

exports.findAllLog = (req, res) => {
    Log.findAll()
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                status : "error",
                message : err.message || "some error while retriiving data"
            })
        })
}