const db = require('../models/index');
const User = db.users;
const Log = db.logs;
const Op = db.Sequelize.Op;
let id_user =  "";

exports.create = (req, res) => {
    if(!req.body.name) {
        res.status(400).send({
            status : "error",
            message : "Content can not empty"
        });

        return;
    }

    const user = {
        name : req.body.name,
        email : req.body.email,
        password : req.body.password,
        address : req.body.address,
        sex : req.body.sex,
        phone : req.body.phone
    }

    User.create(user)
    .then(data => {
        Log.create({
            id_user : data.id_user,
            status : "create new data user" 
        });
        res.send(data);
    })
    .catch(err => {
        Log.create({
            id_user : data.id_user,
            status : "failed create new data user" 
        });
        res.status(500).send({
            status : "error",
            message : err.message || "Some error Ocured while creating the user"
        })
    })
}

exports.findAll = (req,res) => {
    const name = req.query.name;
    var condition = name ? { name : { [Op.iLike]: `%${title}`}} : null;
    User.findAll({ where : condition })
    .then(data => {
        res.send(data)
    })
    .catch( err => {
        res.status(500).send({
            status : "error",
            message: err.message || "error"
        });
    });
}

exports.findOne = (req, res) => {
    const id= req.params.id;

    User.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                status : "error",
                message : err.message | "error"
            });
        });
}

exports.login = (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    User.findOne({ where : { email : email, password : password} })
        .then(data => {
            if(data != null ){
                Log.create({
                    id_user : data.id_user,
                    status : `${req.body.email} login`
                });
                res.send({
                    status : "success",
                    message : "Login successfully"
                });
            }else {
                Log.create({
                    id_user : null,
                    status : `${req.body.email} gagal login`
                });
                res.send({
                    status : "error",
                    message : `email ${email} not registered or password error`
                })
            }
            
        })
        .catch( err => {
            res.send({
                status : "error",
                message : err.message
            })
        })
} 

exports.update = (req, res) => {
    const id = req.params.id;

    User.update(req.body, {
        where: {id_user : id}
    })
        .then(num => {
            if(num == 1){
                Log.create({
                    id_user : id,
                    status : `updated data id = ${id}`
                });
                res.send({
                    status: "success",
                    message : "User was Updated"
                });
            } else {
                Log.create({
                    id_user : id,
                    status : `failed update data id = ${id}`
                });
                res.send({
                    status : "error",
                    message : `Cannot update user with id = ${id}`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                status : "error",
                message : "Error updating user with id " + id
            })
        })
}

exports.delete = (req, res) => {
    const id = req.params.id;

    User.destroy({
        where : { id_user : id}
    })
        .then(num => {
            if(num == 1){
                Log.create({
                    id_user : id,
                    status : `deleted data id = ${id}`
                });
                res.send({
                    status : "success",
                    message : "User was deleted successfully!"
                });
            } else {
                Log.create({
                    id_user : id,
                    status : `failde delete data id = ${id}`
                });
                res.send({
                    message : `Cannot delete User with id = ${id}`
                })
            }
        })
            .catch( err => {
                res.status(500).send({
                    status : "error",
                    message : "Could not Delete User with id ="+id
                })
            })
}