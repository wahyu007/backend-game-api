let db = {
    HOST: "localhost",
    USER: "postgres",
    PASSWORD: "b1l4n0",
    DB: "dblatihan004",
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
}

module.exports = db;