const config = require('../config/config');
const Sequelize = require("sequelize");

// console.log("halo");

const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,
    {
        host: config.HOST,
        dialect: config.dialect,
        operatorsAliases: 0,
        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        },
        logging: true,
    }
);

const Game = require('./games');
const User = require('./users');

Game.associate = function(models) {
    Game.hasMany(models.User, {as : 'player'})
}

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = User(sequelize, Sequelize);
db.games = Game(sequelize, Sequelize);
// db.users = require("./users")(sequelize, Sequelize);
// db.games = require("./games")(sequelize, Sequelize);
db.logs = require("./logs")(sequelize, Sequelize);

// db.games.hasMany(db.users, {
//     foreignKey : 'id_user'
// });

// db.users.belongsTo(db.games);

module.exports = db;
