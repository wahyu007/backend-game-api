module.exports = (sequelize, Sequelize) => {
    const Log = sequelize.define("user_game_history", {
        id_user : {
            type : Sequelize.INTEGER
        }, 
        status : {
            type : Sequelize.TEXT
        }
    });

    return Log;
}