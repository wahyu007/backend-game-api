const { sequelize, Sequelize } = require(".");

module.exports = (sequelize, Sequelize) => {
    const Game = sequelize.define("user_game", {
        id_user : {
            type : Sequelize.INTEGER
        },

        score : {
            type : Sequelize.INTEGER
        }, 

        top_score : {
            type : Sequelize.INTEGER
        }
    });

    return Game;
}