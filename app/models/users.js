module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("user_game_biodata", {
        id_user : {
            type : Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        
        name : {
            type : Sequelize.STRING
        },

        email : {
            type : Sequelize.STRING
        },

        password : {
            type : Sequelize.CHAR
        },

        address : {
            type : Sequelize.STRING
        },

        sex : {
            type : Sequelize.STRING
        },

        phone : {
            type : Sequelize.STRING
        }
    });

    return User;
}